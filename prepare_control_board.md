[Control Board (factory state)]:Parts/DropBot_v2_control_board/Part.md
[UUID qrcode sticker]:Parts/7_5x20mm_QR_code_label/Part.md
* Teensy 3.2 + Header pins (See if Gold Phoenix can solder these)

# Prepare Control Board

{{BOM}}

## Solder (or maybe outsource?)

## Flash firmware



## Get UUID


## Print UUID sticker and stick it on the board

Add a [UUID qrcode sticker]{Qty: 1} and write the board's UUID and i2c address to EEPROM.

## Calibration

* Check that max voltage > 140 (reduce size of resistor)
* Voltage calibration should be <1% error
* Calibrate C16 (should be ~0.33uF; not 0.15uF)

## QC

* Check that max voltage > 140 (reduce size default resistor)
* Feedback calibration limits?