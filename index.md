# DropBot v3

This document contains build instructions for the [DropBot](https://github.com/sci-bots/dropbot-v3) Digital Microfluidics (DMF) automation system. This is the latest generation [DropBot](https://github.com/sci-bots/dropbot-v3) instrument (v3) which makes many improvements over the [previous hardware][DropBot v2]. These systems are available for purchase from [Sci-Bots](https://sci-bots.com).

All hardware and software are open-source, subject to the BSD-3-Clause license (software code) or Creative Commons Attribution-ShareAlike license (hardware designs).

Here's a link to the [Bill of materials]{BOM}.

## Build Guide
1. [.](obtaining-parts.md){step}
1. [.](3d-printing-parts.md){step}
1. [.](cnc-machining-parts.md){step}
1. [.](prepare_switching_boards.md){step}
1. [.](prepare_pogo_pin_board.md){step}
1. [.](prepare_front_panel_board.md){step}
1. [.](assembly.md){step}

The [original DropBot (v2)][DropBot v2] was developed in the [Wheeler Lab at the University of Toronto](http://microfluidics.utoronto.ca/dropbot) and is described in detail in [Fobel et al., Appl. Phys. Lett. 102, 193513 (2013); doi: 10.1063/1.4807118.](http://dx.doi.org/10.1063/1.4807118)

[Dropbot v2]: https://github.com/wheeler-microfluidics/dropbot/wiki