[Pogo-pin Board (factory state)]:Parts/DropBot_v2_pogo_pin_board/Part.md
[UUID qrcode sticker]:Parts/7_5x20mm_QR_code_label/Part.md

# Prepare Pogo-pin Board

{{BOM}}

## Add a UUID sticker

Add a [UUID qrcode sticker]{Qty: 1} and write the board's UUID and i2c address to EEPROM.

## QC test each of the switching boards

Test for shorts and/or non-functioning switches using the on-board diagnostics and a test board.
