# DropBot_v3_40_channel_High_Voltage_Switching_Board

The high-voltage switching boards control the on/off state of electrodes in the [DropBot](https://sci-bots.com/dropbot) system. Each board controls 40-channels and multiple boards can be daisy-chained together.

## Specifications

|Attribute |Value|
|---|---|
|Link|https://github.com/sci-bots/dropbot-40-channel-HV-switching-board.kicad|
