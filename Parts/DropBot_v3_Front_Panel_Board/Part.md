# DropBot_v3_Front_Panel (Factory State)

The front panel board provides the power (+3.3V, +12V) and communication (i2c) bus for the other boards in the DropBot v3 system and routes the high-voltage outputs from the switching boards to the pogo-pin board.

## Specifications

|Attribute |Value|
|---|---|
|Link|https://github.com/sci-bots/dropbot-front-panel.kicad|
