# DropBot_v3_120_Channel_Pogo_Pin_Board (Factory State)

The 120-Channel Pogo-Pin Board routes the high-voltage outputs from the switching boards to a digital microfluidic (DMF) chip via 120 spring-loaded pogo-pins.

## Specifications

|Attribute |Value|
|---|---|
|Link|https://github.com/sci-bots/dropbot-120-channel-pogo-pin-board.kicad|
