# 5.1 Introduction

## Manage data

* csv/xls to .md converter (like Kifield)
* gitbuilding/buildup/markdown
* cadquery (like openSCAD but python and STEP)
* kitspace/3dhubs
* urls for parts
* versioning/packaging for parts? git?
* requirements? can we use open-source solver (e.g., conda)?
* scraping data from mcmaster, digikey, polycase

## Purchase Parts

* purchasing-helpers
* brother-ql

## [Storage boxes](https://github.com/cfobel/storage-boxes)

* [CadQuery](https://github.com/CadQuery/cadquery) fork of the awesome [assortment boxes][0] (video [here][1]) by [Alexandre Chappel][2].
* fork and change size to 50x50x75mm so that they fit in [3D-printer filament boxes](https://www.uline.com/Product/Detail/S-13364/Indestructo-Mailers/8-x-8-x-3-White-Indestructo-Mailers)
* setup CI
* price parts at 3D-hubs

## Links

* [FreeCAD For Beginners p.1 - UI, Sketching, Constraints, Extruding, and 3D Printing](https://www.youtube.com/watch?v=uh5aN_Di8J0)
* [FreeCAD for Beginners pt.7 - Importing .STEP files and Scaling](https://www.youtube.com/watch?v=Ae5O7j-hFwg)
* [FreeCAD Scale a Part with CloneConvert Macro](https://www.youtube.com/watch?v=KLx6G5nhCbo)
* [FreeCAD tutorials playlist by @thehardwareguy](https://www.youtube.com/playlist?list=PLP1rv37BojTd5NY3E_aqOWUe0uA8J-J1T)

[0]: https://www.alch.shop/shop/p/assortment-boxes-v2
[1]: https://youtu.be/VntGnLuwoeY
[2]: https://www.alch.shop/