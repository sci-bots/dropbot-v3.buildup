[40-channel High Voltage Switching Boards (factory state)]:Parts/DropBot_v3_40_channel_High_Voltage_Switching_Board_factory_state/Part.md
[AVR ISP Programmer]:Parts/Arduino_Uno_AVR_ISP_Programmer_Shield/Part.md "{cat:tools}"
[UUID qrcode sticker]:Parts/7_5x20mm_QR_code_label/Part.md

# Prepare High-Voltage Switching Boards

{{BOM}}

Each DropBot requires three [40-channel High Voltage Switching Boards (factory state)]{Qty: 3},
but before these boards can be added to a system, we need to flash a bootloader/firmware and
assign them a unique ID.

## Flashing the twi bootloader

Flash the twibootloader using an [AVR ISP Programmer]{Qty: 1}

1. Add avrdude to the system path with the following terminal command: `set PATH=%PATH%;%LOCALAPPDATA%\MicroDrop\app-3.1.3\app\share\platformio\packages\tool-avrdude`
2. Download the `hv-switching-board-firmware-v0.10-twiboot.hex` and `eeprom-address32.hex` files from [github](https://github.com/sci-bots/hv-switching-board-firmware/releases).
3. Change to the directory where you downloaded the files using the `cd` command 
4. Run the following command to set the fuses on the microcontroller (making sure to swap the COM port appropriately): `avrdude -P <com port> -b 19200 -c avrisp -p m328p -v -U lfuse:w:0xEF:m -U hfuse:w:0xD4:m -U efuse:w:0xFD:m`
4. Run the following command to flash the firmware and EEPROM (again swapping the COM port appropriately): `avrdude -P <com port> -b 19200 -c avrisp -p m328p -v -e -V -U flash:w:hv-switching-board-firmware-v0.10-twiboot.hex:i -U eeprom:w:eeprom-address<i2c address>.hex:i`

## Flashing firmware over i2c

Use the DropBot to flash the firmware over i2c.

https://gitlab.com/sci-bots/dropbot.py/-/blob/master/dropbot/notebooks/tool(upload)%20-%20flash%20HV%20switching%20board%20firmware.ipynb

## Add a UUID sticker and setting the board ID

Add a [UUID qrcode sticker]{Qty: 1} and write the board's UUID and i2c address to EEPROM.

## QC test each of the switching boards

Test for shorts and/or non-functioning switches using the on-board diagnostics and a test board.
