[Front Panel Board]:Parts/DropBot_v3_Front_Panel_Board/Part.md
[QR Code Sticker]:Parts/7_5x20mm_QR_code_label/Part.md
[+3.3V Step-Down Voltage Regulator]:Parts/D24V22F3/Part.md
[+12V power cable]:Parts/+12V_Power_Cable/Part.md
[Kapton Tape]:Parts/Kapton_tape/Part.md "{cat:tools}"
[Hot Glue Gun]:Parts/Hot_Glue_Gun/Part.md "{cat:tools}"
[Soldering Iron]:Parts/Soldering_Iron/Part.md "{cat:tools}"
[Lead-free Solder]:Parts/Lead-Free_Solder/Part.md "{cat:tools}"
[Side Cutters]:Parts/Side_cutters/Part.md "{cat:tools}"

# Prepare Front Panel Board

{{BOM}}

Several post-processing steps need to be applied to the
[Front Panel Board]{Qty: 1} before it is ready to add to a new
DropBot system.

## Solder the +12V power cable and +3.3V Step-Down Voltage Regulator

Solder the [+3.3V Step-Down Voltage Regulator]{Qty: 1} and
[+12V power cable]{Qty: 1} to the [Front Panel Board] as shown in the
following photo using a [Soldering Iron]{Qty: 1} and [Lead-free Solder]{Qty: 1}.

![](Parts/DropBot_v3_Front_Panel_Board/solder-power-regulator-and-cable.png)

Cut the pins and power cable wires flush using a pair of [Side Cutters]{Qty: 1}.

![](Parts/DropBot_v3_Front_Panel_Board/cut-pins-with-sidecutters.png)

## Reinforce solder joints on the power supply choke

Apply additional [Lead-free Solder] to the power supply choke pins on the front
of the board to provide mechanical reinforcement.

![](Parts/DropBot_v3_Front_Panel_Board/apply-extra-solder-to-choke.png)

## Add a UUID sticker

Apply a [QR Code Sticker]{Qty: 1} as shown.

![](Parts/DropBot_v3_Front_Panel_Board/apply-uuid-sticker.png)

## Assign a UUID to the board
 
Assign the UUID matching the [QR Code Sticker] using the following Python code:

```python
import uuid
from dropbot import SerialProxy, metadata
import numpy as np

uuid_ = uuid.UUID('PASTE_UUID_CODE_HERE')

try:
    proxy.terminate()
except:
    pass

proxy = SerialProxy()
print(proxy.properties)

def assign_uuid(uuid_):
    board = metadata.Hardware(uuid=uuid_.bytes,
                                name='dropbot-front-panel',
                                version='3.3')
    i2c_address = 81

    if i2c_address in proxy.i2c_scan():
        # write the protocol buffer to the eeprom
        data = np.fromstring(board.SerializeToString(), dtype=np.uint8)
        proxy.i2c_eeprom_write(i2c_address, 0, [len(data)] + data.tolist())

        # read it back
        n_bytes = proxy.i2c_eeprom_read(i2c_address, 0, 1)
        data = proxy.i2c_eeprom_read(i2c_address, 1, n_bytes)
        board = metadata.Hardware.FromString(data.tobytes())
        print "\n%s v%s, uuid: %s" % (board.name, board.version,
            uuid.UUID(bytes=board.uuid))

assign_uuid(uuid_)
```

## QC test the front panel board

Test for shorts and/or disconnected channels using the DropBot's on-board diagnostics and a test board.

## Apply kapton tape to the back of the board

If the board passes QC, apply several strips of [Kapton Tape]{Qty: 1}
to the back side of the board to prevent accidental shorts.

![](Parts/DropBot_v3_Front_Panel_Board/apply-kapton-tape.png)

## Apply hot glue

Use a [Hot Glue Gun]{Qty: 1} to apply a blob of glue to the front of the board
where the [+12V power cable] is attached to mechanically support this connection.
Also apply a bead of hot glue around the perimeter of the power supply choke.
**Be sure to leave clearance for the switching boards**.

![](Parts/DropBot_v3_Front_Panel_Board/apply-hot-glue.png)